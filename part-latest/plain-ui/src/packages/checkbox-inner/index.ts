import inner from './checkbox-inner'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(inner)