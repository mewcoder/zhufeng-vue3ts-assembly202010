import scroll from './scroll'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(scroll)