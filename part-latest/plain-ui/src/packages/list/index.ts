import list from './list'
import {createComponentPlugin} from "../../utils/createComponentPlugin";

export default createComponentPlugin(list)