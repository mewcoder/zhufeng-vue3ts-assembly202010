# PART03

## 上节回顾

在 PART02 中，我们已经写好了 `designComponent` 函数，并且定义的组件，
能够暴露 `use.ref` 来获取组件的引用以及获取引用类型。
在这里我们先填一下上一次挖的坑，在 `designComponent` 函数中，
其中的 `use.ref` 我们当时是这么写的：

```ts
ref: (refName: string) => {
    const ctx = (getCurrentInstance() as any).ctx
    return { 
        get value() {
            return ((ctx as any).$refs[refName].$._refer) as Refer | null
        }
    }
}
```

> 当打包组件，或者打包页面的时候，这个 `getCurrentInstance().ctx.$refs` 实际上是不存在的。非常感谢群里同学们的积极研究。后来我才知道只需要将refer挂载到 `getCurrentInstance().proxy`里面，这样在使用官方的做法（this.$refs[refName]）获取组件引用的时候，代理对象里面就有了正确的内容。怪我的思维仍然局限在Vue2.0中，一直想的还是挂载到组件的实例上下文对象里边。

在 `designComponent`中，我们需要将：
- `ctx._refer = refer`这句代改成以下内容：
    ```tsx
    if (!!refer) {
        const duplicateKey = Object.keys(leftOptions.props || {})
            .find(i => Object.prototype.hasOwnProperty.call(refer as any, i))
        if (!!duplicateKey) {
            console.error(`designComponent: duplicate key ${duplicateKey} in refer`)
        } else {
            Object.assign(ctx.proxy, refer)
        }
    }
    ```
- 以及下面的`use.ref`：
    ```tsx
    ref: (refName: string) => {
        const ctx = getCurrentInstance()!
        return {
            get value() {
                return ctx.refs[refName] as Refer | null
            }
        }
    },
    ```

这样就好啦，其他代码应该都是不需要改的。


# 正文

> 提示；请不要一边看着文档一边照着做，先看完，理解一遍本次文档的核心内容，带着自己的理解以及疑问开始做。

## 一、自定义事件

### 1、概述

- 在 Vue3.0 中，只能派发事件，`$on, $off, $once`已经全部移除，但是我们在开发组件的时候，仍然需要组件内部的事件监听机制；所以我们自己模拟这一种机制。
- 其次，我们在定义组件的时候，需要将可能派发的事件，通过 `emits` 选项传递给 `defineComponent`。当用户在监听组件的事件的时候，没有定义在 `emits` 中的事件，会自动监听到组件的根节点上，如果是多个根节点，则监听的事件无效，并且控制台发出警告。
- 有的同学可能看过 `plain-ui`的源码中的`useEvent`组合函数，实际上 `useEvent`更多的作用是类型提示以及规范组件中派发事件的写法。每个组件，都需要将可能派发的事件定义在 `useEvent`中，每个事件派发的参数类型都是预先定义好的。

### 2、意义

先来看一段代码：

```ts
const scroll = Scroll.use.inject('srcoll')

scroll.event.on.scroll(handler.scroll)
onBeforeUnmount(()=> scroll.event.off.scroll(handler.scroll))
```

通过这种方式，监听组件事件以及取消监听事件能够通过 typescript 类型约束的帮助，可以使得我们的组件代码更加健壮。比如 Scroll 组件内部在定义 scroll 事件的时候，指定了派发的参数只有一个 `scrollTop:number` 值，此时如果 `handler.scroll`函数接收的参数没有写对，那么 `scroll.event.on.scroll(handler.scroll)` 是无法通过类型检查的 。 

### 3、实现

#### src/plugins/Event.ts

> 因为我们需要自己实现事件监听派发机制，所以我们需要一个辅助的工具对象帮助我们实现这么一个功能。`Event.ts`是一个事件派发以及监听工具函数，会生成类型为 `{on, once, off, emit}` 的这么一个对象，当我们组件内部派发事件的同时，还需要调用这个对象的 `emit`方法派发事件。
这个功能有点类似于[mitt](https://www.npmjs.com/package/mitt)；

```ts
import {SimpleFunction} from "../shims";

type Listener = (SimpleFunction & { fn?: any })
type ListenName = string | symbol
export type PlainEvent = ReturnType<typeof createPlainEvent>

export function createPlainEvent() {

    /**
     * 第一次调用getEvents的时候才创建map对象
     * @author  韦胜健
     * @date    2020/10/17 11:27
     */
    const getListenMap = (() => {
        let events: Map<ListenName, Listener[]>;
        return () => {
            if (!events) {
                events = new Map<ListenName, Listener[]>()
            }
            return events
        }
    })()

    const event = {
        on: (listenName: ListenName, fn: SimpleFunction) => {
            const listenMap = getListenMap()
            const map = listenMap.get(listenName)
            if (!!map) {
                map.push(fn)
            } else {
                listenMap.set(listenName, [fn])
            }
            return () => event.off(listenName, fn)
        },
        once: (listenName: ListenName, fn: SimpleFunction) => {
            const on: Listener = (...args: any[]) => {
                event.off(listenName, fn)
                fn(...args)
            }
            on.fn = fn
            event.on(listenName, on)
            return () => event.off(listenName, on)
        },
        off: (listenName: ListenName, fn?: SimpleFunction) => {
            const listenMap = getListenMap()

            const listeners = listenMap.get(listenName)
            if (!listeners) {
                return;
            }

            /*移除listenName的所有监听器*/
            if (!fn) {
                listenMap.set(listenName, [])
                return
            }

            for (let i = 0; i < listeners.length; i++) {
                const listener = listeners[i];
                if (fn === listener || (!!listener.fn && fn === listener.fn)) {
                    listeners.splice(i, 1)
                    break
                }
            }
        },
        emit: (listenName: ListenName, ...args: any[]) => {
            const listeners = getListenMap().get(listenName)
            if (!!listeners) {
                listeners.forEach(listener => listener(...args))
            }
        },
    }

    return event
}
```

#### src/use/useEvent.ts

在 `plain-ui`中，经常会看见组件`setup`中使用了 `useEvent`函数定义组件内部派发的事件，比如这里[https://gitee.com/plain-pot/plain-ui/blob/v2.0-latest/src/packages/input/input.tsx](https://gitee.com/plain-pot/plain-ui/blob/v2.0-latest/src/packages/input/input.tsx)，由于在 Vue3.0 中自定义组件的事件需要全部定义在 `emits`选项中([自定义事件](http://martsforever-snapshot.gitee.io/vue-docs-next-zh-cn/guide/component-custom-events.html#%E4%BA%8B%E4%BB%B6%E5%90%8D))，所以我们这一次封装好 `useEvent.ts`之后，直接整到 `designComponent` 中。

```ts
import {getCurrentInstance} from 'vue'
import {kebabCase} from "../utils/kebabCase";
import {createPlainEvent} from "../plugins/Event";
import {SimpleFunction} from "../shims";

// focus                -> focus
// itemClick            -> item-click
// updateModelValue     -> update:modelValue
// updateStart          -> update:start
function emitName2ListenName(emitName: string): string {
    const match = emitName.match(/update([A-Z])(.*)/)
    if (match) {
        return `update:${match[1].toLowerCase()}${match[2]}`
    }
    return kebabCase(emitName)!
}

type EventListener<EmitsValue> = EmitsValue extends (...args: any[]) => any ? Parameters<EmitsValue> : never

export type ComponentEvent<Emit> = {
    emit: { [key in keyof Emit]: (...args: EventListener<Emit[key]>) => void },
    on: { [key in keyof Emit]: (cb: (...args: EventListener<Emit[key]>) => void) => void },
    once: { [key in keyof Emit]: (cb: (...args: EventListener<Emit[key]>) => void) => void },
    off: { [key in keyof Emit]: (cb: (...args: EventListener<Emit[key]>) => void) => void },
}

export function getComponentEmit<T>(emitObject: T): T {
    return {
        change: null,
        ...Object.keys(emitObject || {}).reduce((ret: any, key: string) => {
            ret[emitName2ListenName(key)] = (emitObject as any)[key]
            return ret
        }, {} as any),
    }
}

export function useEvent<T>(emitObject: T): ComponentEvent<T> {

    const ctx = getCurrentInstance()!
    const event = createPlainEvent()

    const emit = {} as any;
    const on = {} as any;
    const once = {} as any;
    const off = {} as any;


    Object.keys(emitObject || {}).forEach(key => {
        /*派发事件名称，横杠命名*/
        const kebabCaseName = emitName2ListenName(key)

        emit[key] = (...args: any[]) => {
            ctx.emit(kebabCaseName, ...args)
            event.emit(kebabCaseName, ...args)
            if (key === 'updateModelValue') {
                ctx.emit('change', ...args)
                event.emit('change', ...args)
            }
        }
        on[key] = (fn: SimpleFunction) => event.on(kebabCaseName, fn)
        once[key] = (fn: SimpleFunction) => event.once(kebabCaseName, fn)
        off[key] = (fn: SimpleFunction) => event.off(kebabCaseName, fn)
    })

    return {
        emit, on, once, off
    } as any
}
```

#### src/use/designComponent.ts

修改 `deisgnComponent.ts`文件，在 `options` 参数中多接收一个 `emits` 选项，在调用 `options.setup` 之前，使用 `useEvent`函数创建 `event` 对象，然后在调用 `options.setup`的时候，将这个 event 对象作为参数传递给组件使用。

```ts
import {ComponentPropsOptions, ExtractPropTypes, SetupContext, defineComponent, getCurrentInstance, provide, inject} from 'vue';
import {ComponentEvent, getComponentEmit, useEvent} from "./useEvent";

export function designComponent<PropsOptions extends Readonly<ComponentPropsOptions>,
    Props extends Readonly<ExtractPropTypes<PropsOptions>>,
    Emits extends { [k: string]: (...args: any[]) => boolean },
    Refer,
    >(
    options: {
        name?: string,
        props?: PropsOptions,

        provideRefer?: boolean,
        emits?: Emits,
        setup: (parameter: { props: Props, event: ComponentEvent<Emits>, setupContext: SetupContext<Emits> }) => {
            refer?: Refer,
            render: () => any
        }
    }) {

    const {setup, provideRefer, emits, ...leftOptions} = options

    return {
        ...defineComponent({
            ...leftOptions,
            emits: getComponentEmit(emits),
            setup(props: Props, setupContext: any) {

                const ctx = getCurrentInstance()!
                const event = useEvent<Emits>(emits!)

                if (!setup) {
                    console.error('designComponent: setup is required!')
                    return () => null
                }

                const {refer, render} = setup({props, event, setupContext})
                if (!!refer) {
                    const duplicateKey = Object.keys(leftOptions.props || {})
                        .find(i => Object.prototype.hasOwnProperty.call(refer as any, i))
                    if (!!duplicateKey) {
                        console.error(`designComponent: duplicate key ${duplicateKey} in refer`)
                    } else {
                        Object.assign(ctx.proxy, refer)
                    }
                }

                if (provideRefer) {
                    if (!leftOptions.name) {
                        console.error('designComponent: name is required when provideRefer is true!')
                    } else {
                        provide(`@@${leftOptions.name}`, refer)
                    }
                }

                return render
            },
        } as any),
        use: {
            ref: (refName: string) => {
                const ctx = getCurrentInstance()!
                return {
                    get value() {
                        return ctx.refs[refName] as Refer | null
                    }
                }
            },
            inject: (defaultValue?: any) => {
                return inject(`@@${leftOptions.name}`, defaultValue) as Refer
            }
        }
    }
}
```

### 4、作业

按照上面给出的关键代码，实现以下内容：

#### 1、根据给出的关键代码完成 designComponent 函数，使其支持通过emits选项定义派发的事件（事件名为驼峰命名，派发事件时转化为横杠命名），并且在setup函数参数中获取event对象用来派发以及监听事件；

剩余的相关代码：

**kebabcase**

```ts
export const kebabCase = (str: string | null): string | null => {
    if (!str) return null
    if (str.length > 1 && /[A-Z]/.test(str.charAt(0))) {
        str = str.charAt(0).toLowerCase() + str.substring(1)
    }
    return str.replace(/[A-Z]/g, (i) => '-' + i.toLowerCase())
}
```

**shims.ts**

```ts
export type SimpleFunction = (...args: any[]) => any
```

#### 2、使用整合了`useEvent`的 `designComponent` 函数重构以下两个组件

##### Button

点击`button`节点的时候，派发`click` 事件；

##### Input

实现双向绑定功能：

> 提示：
> - 定义一个临时变量：modelValue = ref(props.modelValue)
> - 将 `modelValue.value` 传递给input元素的value属性；
> - 监听input元素的input事件，事件处理函数中，更新 modelValue.value 以及调用 `event.emit.updateModelvalue(modelValue.value)`派发事件`update:modelValue`更新绑定值；
> - watch 监听 props.modelValue，当值变化时，更新 modelValue.value 为新值。

> 思考：
> - 如果不使用临时变量，会发生什么事情？
> - 参考 ElementUI 的 ElInput 输入框组件，在不使用 v-model 绑定值时，Input输入框的表现；

##### 其他

因为我们修改了designComponent的setup类型，原本参数是 `(props,setupContext)`，现在改成了
`(parameter:{props,setupContext,event})`，所以修改项目中已经使用了 `designComponent`的组件。通过解构的方式获取 `props,setupContext`。

> 因为我们后续基本不使用 setupContext，所以这里通过解构的方式获取会方便一点；

#### 3、尝试在不借助任何帮助的情况下，从0开始实现 `designComponent, Event, useEvent`这三个文件的实现。

加深理解，锻炼自己typescript撰写能力。

#### 4、（可选作业）实现如下效果

> 如果你想变得更强，那么这个作业就是必做题。

先来看要实现的效果：

![demo-use-event.gif](./demo-use-event.gif)

[示例页面: http://zhufeng-training.gitee.io/v3-components-wsj/#%2Fuse%2FuseEvent%2Fdemo-use-event](http://zhufeng-training.gitee.io/v3-components-wsj/#%2Fuse%2FuseEvent%2Fdemo-use-event)

- 一共是三个组件：DemoUseEventTable, DemoUseEventTableBody, DemoUseEventTableHead
- 开发者仅能使用 DemoUseEventTable，其他两个组件是 DemoUseEventTable的内部组件，开发者是接触不到的。
- 这里模拟实现了 表头固定，表体滚动的简单实现。
- 在不影响鼠标滚轮滚动、触摸板滚动的情况下，实现：1）横向联动滚动，2）在表头使用使用鼠标滚动横向滚动，3）表体中使用 alt+鼠标滚动 实现横向滚动；

这里给出 `DemoUseEventTableHead` 的源码，其他的为作业实现；

```tsx
import {designComponent} from "../../../../src/use/designComponent";
import {DemoUseEventTable, DemoUseEventTablePart} from "./DemoUseEventTable";
import {onBeforeUnmount, ref} from 'vue';

export const DemoUseEventTableHead = designComponent({
    setup() {

        const wrapperEl = ref(null as null | HTMLDivElement)
        const table = DemoUseEventTable.use.inject()

        const handler = {
            /**
             * 组件销毁的时候，如果不取消监听事件，则监听的事件一直有效
             * 此时 wrapperEl  节点已经销毁，会导致一直报错
             * @author  韦胜健
             * @date    2020/10/29 16:14
             */
            scroll: (e: Event, part: DemoUseEventTablePart) => {
                // console.log('head handle scroll', Date.now())
                if (part === DemoUseEventTablePart.body) {
                    wrapperEl.value!.scrollLeft = (e.target as HTMLDivElement).scrollLeft
                }
            },
            wrapperScroll: (e: Event) => {
                if (table.state.hoverPart === DemoUseEventTablePart.head) {
                    table.event.emit.scroll(e, DemoUseEventTablePart.head)
                }
            },
            mousewheel: (e: MouseWheelEvent) => {
                wrapperEl.value!.scrollLeft = wrapperEl.value!.scrollLeft + e.deltaY
            }
        }

        table.event.on.scroll(handler.scroll)
        onBeforeUnmount(() => table.event.off.scroll(handler.scroll))

        return {
            render: () => (
                <div class="demo-use-event-table-head"
                     ref={wrapperEl}
                     onScroll={handler.wrapperScroll}
                     {...{onMousewheel: handler.mousewheel}}>
                    <div class="demo-use-event-table-head-inner">
                        table head
                    </div>
                </div>
            )
        }
    },
})
```

## 结语

- 本次训练已经给出关键代码，后续在训练实现其他的组合函数时，将不再提供组合函数关键代码，而是作为答案给出，希望同学们能够多动手自己实现，思考如何实现组件开发中常用的组合函数。
- 本次训练依据同学们的表现而定，如果有需要，将录制本次PART03的训练视频，在周六晚上作为答案公布。