# Vue组件库训练营

## 训练营说明

- 本训练营每两天发布一次文档以及作业。
- 当前为Vue组件库训练营第一期，第一期讲的内容主要是设计组合函数来约束组件开发流程，同时也会搭配一些组件的实现原理来理解组合函数；

## 相关资料

- [组件库成品](http://plain-pot.gitee.io/plain-ui-doc/#home%2Fintroduce)：这个组件库还是基于Vue2.0开发，不过使用的是 `@vue/composition-api + typescript`，所以每个组件的实现原理与 `Vue3.0`大致相同；
- [Vue3.0官方文档（英文）](https://v3.vuejs.org/guide/introduction.html)
- [Vue3.0官方文档（中文）](https://v3.cn.vuejs.org/guide/introduction.html)
- [渲染函数：官方文档](https://v3.vuejs.org/guide/render-function.html#jsx)
- [渲染函数：jsx-next github](https://github.com/vuejs/jsx-next#installation)
- [Vue3.0文档（中文）](http://martsforever-snapshot.gitee.io/vue-docs-next-zh-cn/)：我在码云上同步过来的中文文档，不用翻墙，访问快很多；
- [Composition Api](http://martsforever-snapshot.gitee.io/vue-docs-next-zh-cn/guide/composition-api-introduction.html)：在Vue3.0文档中一样可以找到，这里给出直接访问地址。
- [Typescript Deep Dive](http://martsforever-snapshot.gitee.io/typescript-book-chinese/)：我在码云上同步过来的 `Typescript Deep Dive`一书的中文文档，不用翻墙访问很快；
- [@vue/cli Vue官方脚手架](https://cli.vuejs.org/zh/)：官方推荐的用于创建Vue工程脚手架工具
- [Vite](https://www.npmjs.com/package/vite)：尤雨溪大佬新出的，旨在替代webpack-dev的开发工具；

## 技能要求

- 熟悉Vue开发规范，及其响应式原理；
- 熟悉typescript；
- 熟悉sass、less、stylus其中至少一种预处理语言，本次训练营以sass为主；
- 熟悉vue-cli配置文件；
- 熟悉Webpack打包编译、了解 `umd,commonjs,es module`等规范；
- 熟悉markdown基础语法，编写作业文档；

## 入营须知

- 需要参加的小伙伴必须把学号和姓名添加到表格中:[https://shimo.im/sheets/TdcHWCw68Gw88CjC/MODOC/](https://shimo.im/sheets/TdcHWCw68Gw88CjC/MODOC/)；
- 本期训练营一共两周，每两天发布一次文档以及作业；每两天交一次作业作为一个阶段的任务；
- 参加本训练营需要支付7学分；
- 发布作业的当天晚上8点布置实践任务，截止两天后晚上8点前完成提交作业；完成阶段任务，每part可依次获得 1、2、3、4、5、6、7个学分；
- 如果当天任务无法完成，则中止训练营资格，请自动离群，无法参与后续的学习，已经扣除的学分不退机会只有一次，希望把握住
- 全部学员按学号尾号分为10个组,尾号相同的为一组,当天小组成员全部完成的每个人的学分翻倍
- 在其期间因为某些原因不能参加训练营的小伙伴们要私聊助教

## 训练流程

1. 先将训练营的仓库代码仓库 Fork 到自己的码云账号下：[https://gitee.com/speedly_admin/zhufeng-vue3ts-assembly202010](https://gitee.com/speedly_admin/zhufeng-vue3ts-assembly202010)
2. 将 Fork 后的仓库 Clone 到本地
3. 在项目根目录下创建自己的项目并完成查看README.md文档
4. 把实践总结(学到了什么？收获了什么?遇到了什么问题?如何解决的?)写在当天目录的学习日记.md文件里
5. 在本地仓库完成作业后，push 到自己的码云远程仓库中
6. 最后将自己最后的commit链接地址添加到训练营仓库的当天issue中
7. 完成后在微信群中打卡，并@助教表示完成，在自己小组里可以@组长表示完成，组长可以在表格里添加学分，全员晚八点之前完成就可以进行翻倍，前提是组长需要向助教截图
8. 大家如果遇到问题可以在群里讨论的

如果不会操作的可以看操作视频:[http://img.zhufengpeixun.cn/submitwork.mp4](http://img.zhufengpeixun.cn/submitwork.mp4)