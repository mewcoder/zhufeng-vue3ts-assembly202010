# DAY02

本次实践的主要内容是编写一个简易的页面路由组件，无需配置路由，简化页面开发流程；

## 开始

### 一、.eslintrc

接着DAY01的进度，我们首先编辑根目录下的`.eslintrc`文件，将其中的rules节点内容替换，如下所示，其实就是增加了一些选项，忽略部分eslint规则，大家可以根据自己的喜好，或者公司的eslint规范调整；

```js
module.exports = {
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/no-use-before-define': 'off',
        '@typescript-eslint/member-delimiter-style': 'off',
        'no-extra-boolean-cast': 'off',
        '@typescript-eslint/no-unused-vars': 'off',
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/ban-ts-ignore': 'off',
        'prefer-const': 'off',
    }
}
```

### 二、配置启动页面

#### 1、删除 src 目录下所有文件，除了 `shims-vue.d.ts`；

有的同学可能会发现这个文件里面有报错，因为里面多了一个 type，删除了就好了。最终内容是这样的：

```ts
declare module '*.vue' {
    import {DefineComponent} from 'vue'
    const component: DefineComponent<{}, {}, any>
    export default component
}
```

#### 2、在根目录下新建 story 文件夹

在story文件夹下添加 `app.vue`文件，内容如下：

```html
<template>
    <div>
        hello world
    </div>
</template>

<script>
    export default {
        name: "app"
    }
</script>

<style lang="scss">

</style>
```

在story文件夹下添加 `main.ts`文件，内容如下：

```ts
import App from './app.vue'
import {createApp} from 'vue'

const app = createApp(App)
app.mount('#app')
```

这时候这句引入代码应该会报错：`import App from './app.vue'`，不要着急，现在配置一下 `tsconfig.json`文件在，在其中的`include`节点添加`story`文件夹，结果是这样的：

```json
{
    "include": [
        "src/**/*.ts",
        "src/**/*.tsx",
        "src/**/*.vue",
        "story/**/*.ts",
        "story/**/*.tsx",
        "story/**/*.vue",
        "tests/**/*.ts",
        "tests/**/*.tsx"
    ]
}
```

现在报错应该没有了，接下来是配置以 `story/main.ts`为启动页面；修改根目录下的 `vue.config.js`，内容如下：

```js
const path = require('path')
const resolve = filePath => path.join(__dirname, './', filePath)

module.exports = {
    outputDir: 'docs',
    publicPath: '/v3-components/',
    devServer: {port: '3366'},
    pages: {
        index: {
            entry: resolve('story/main.ts'),
            template: 'public/index.html',
            filename: 'index.html',
            title: 'v3-components',
        },
    },
    chainWebpack: config => {
        config.plugins
            .delete('prefetch-index')
            .delete('preload-index');
        config.resolve.alias
            .set('story', resolve('story'))
    }
}
```

接着 `npm run serve`就可以启动了，启动后看见简洁的`hello world`表明上述配置成功。

### 三、答疑环节

#### 1、shims-vue.d.ts作用是什么？

作业；

#### 2、为什么要配置启动页面为 `story/main.ts`，为什么不能放置在 `src`目录下？

组件库工程总的来说，分为两部分，一部分是组件源码，另一部分是测试组件的demo页面，可以学习ElementIUI将组件源码放在根目录的`packages`文件夹中，但小编感觉将组件源码放`src`更合适，因为别人在浏览你的源码的时候，一般第一反应就是去看src目录下的文件内容，你的库的入口一般都是设计为src目录下的index.ts或者index.js；

---

将demo放在story文件夹下面后，需要配置`vue.config.js`的index单页面入口文件为`story/main.ts`；因为`vue-cli`默认是找`src/main.ts`作为入口文件。

---

后续我们打包组件库的时候，会以`src/index.ts`为入口，以umd格式打包所有组件为一个文件。当用户不需要按需引入的时候，直接引入这个总的文件就好了。我们在publish的时候，除了这个打包后的总的文件，还需要将src文件夹publish到npm仓库中，也就是说，我们需要把组件源码publish上去。这样当用户按需引入的时候，通过 `babel-plugin-import`按需引入其中的组件源码，这样可以最大化减少冗余代码，并且由用户自己管理兼容浏览器所需要的polyfill；

#### 3、vue.config.js 里面 chainWebpack做了什么？

可以看到，删除了`prefetch-index`以及`preload-index`两个插件，这个两个插件会在我们的index单页面中的script标签加上 defer以及async属性，导致我们demo示例页面按需加载失效，去掉就好了。

### 四、路由组件

#### 1、创建story目录结构

- story
    - components：示例页面需要的一些组件，比如路由组件、示例分类组件、页面锚地组件等等；
    - pages：用来存放组件示例页面，同时也是路由的根目录；
        - normal：普通组件示例页面存放目录，比如button、input、icon等等；
            - button.vue
            - color.vue
            - icon.vue
            - layout.vue
        - form：表单示例页面存放目录；
        - table：表格示例页面存放目录；
        - service：服务示例页面存放目录；
        - ......
    - ......

> 省略号表示还有一些文件或者文件夹，但是目前不需要；

按照上述的目录结构，创建 `button.vue, color.vue, icon.vue, layout.vue`四个示例页面；

#### 2、实现路由组件

路由一个由两个组件组成：

- story/components/navigator/app-navigator.tsx：无根节点组件，负责计算`window.localtion.hash`中的路由信息（这里只做哈希路由，不做那么复杂，怎么简单怎么来）；并且将路由信息以及一些路由跳转方法工具对象provide给子组件；
- story/components/navigator/app-navigator-page.tsx：负责读取`app-navigator`中的路由信息，根据路由地址按需加载组件示例页面；

其他的，比如首页菜单组件，只需要inject注入 app-navigator提供的对象，当点击菜单跳转的时候调用对象的路由跳转方法即可；

**app-navigator.tsx**

```jsx
import {defineComponent, reactive, onBeforeUnmount, provide, inject} from 'vue'

interface Route {
    path?: string
    hash?: string
    param?: { [k: string]: string }
}

const APP_NAVIGATOR_PROVIDER = '@@app-navigator'

/**
 * hash路由
 * @author  韦胜健
 * @date    2020/10/22 11:05
 */
function getRoute(): Route {
    let locationHash = window.location.hash || ''
    if (locationHash.charAt(0) === '#') {
        locationHash = locationHash.slice(1)
    }
    const [path, hash] = (decodeURIComponent(locationHash)).split('#')
    return {
        path,
        hash,
    }
}

function useAppNavigator(props: { defaultPath?: string }) {

    const currentRoute = getRoute();
    !currentRoute.path && (currentRoute.path = props.defaultPath);

    const state = reactive({
        route: currentRoute
    })

    const methods = {
        go(path: string) {
            window.location.hash = encodeURIComponent(path)
        },
    }

    const handler = {
        hashchange: () => {
            state.route = getRoute()
        }
    }

    const refer = {
        state,
        methods,
    }

    window.addEventListener('hashchange', handler.hashchange)
    onBeforeUnmount(() => window.removeEventListener('hashchange', handler.hashchange))

    provide(APP_NAVIGATOR_PROVIDER, refer)

    return refer
}

export function injectAppNavigator() {
    return inject(APP_NAVIGATOR_PROVIDER) as ReturnType<typeof useAppNavigator>
}


export const AppNavigator = defineComponent({
    name: 'app-navigator',
    props: {
        defaultPath: String,
    },
    setup(props, setupContext) {

        useAppNavigator(props)

        return () => !!setupContext.slots.default ? setupContext.slots.default() : null
    },
})
```

**app-navigator-page.tsx**

```jsx
import {defineComponent, reactive, DefineComponent, markRaw, watch} from "vue";
import {injectAppNavigator} from "./app-navigator";

export const AppNavigatorPage = defineComponent({
    setup() {

        const state = reactive({
            PageComponent: null as null | DefineComponent,
        })

        const navigator = injectAppNavigator()

        const utils = {
            reset: async () => {
                let {path} = navigator.state.route
                if (!path) {
                    return
                }
                if (path.charAt(0) === '/') {
                    path = path.slice(1)
                }
                const Component = (await import('story/pages/' + path)).default
                state.PageComponent = markRaw(Component)
            }
        }

        watch(() => navigator.state.route.path, utils.reset, {immediate: true})

        return () => {

            const {PageComponent} = state

            return (
                <div class="app-navigator-page">
                    {!!PageComponent ? <PageComponent/> : null}
                </div>
            )
        }
    },
})
```

修改 app.vue,使用这两个路由组件：

```html
<template>
    <app-navigator defaultPath="normal/button">
        <div class="app-home">
            <app-navigator-page/>
        </div>
    </app-navigator>
</template>

<script>
    import {AppNavigator} from "./components/navigator/app-navigator";
    import {AppNavigatorPage} from "./components/navigator/app-navigator-page";

    export default {
        components: {
            AppNavigator,
            AppNavigatorPage,
        },
        name: "app"
    }
</script>

<style lang="scss">

</style>
```

这样这两个路由组件算是开发好了，当地址栏输入：`http://localhost:3366/v3-components/#normal/icon`能够看见`icon.vue`中的内容，地址栏输入`http://localhost:3366/v3-components/#normal/color`能够看见`color.vue`中的内容，此时表明路由已经能够正常加载页面；

#### 3、完善示例首页

**app.vue代码结构**

```html
<template>
    <app-navigator defaultPath="normal/button">
        <article class="app-home">
            <section class="app-header" :style="{height:HomeConfig.headSize+'px',}">
                v3-component
            </section>
            <section class="app-menu" :style="{width:HomeConfig.menuSize+'px',top:HomeConfig.headSize+'px'}">
                <app-menu/>
            </section>
            <section class="app-content" :style="{
                paddingTop:(HomeConfig.headSize+20)+'px',
                paddingLeft:(HomeConfig.menuSize+20)+'px',
            }">
                <app-navigator-page/>
            </section>
        </article>
    </app-navigator>
</template>

<script>
    import {AppNavigator} from "./components/navigator/app-navigator";
    import {AppNavigatorPage} from "./components/navigator/app-navigator-page";
    import AppMenu from "./components/app/app-menu";

    const HomeConfig = {
        headSize: 60,
        menuSize: 300,
    }

    export default {
        components: {
            AppMenu,
            AppNavigator,
            AppNavigatorPage,
        },
        name: "app",
        data() {
            return {
                HomeConfig,
            }
        },
    }
</script>

<style lang="scss">

    html, body {
        margin: 0;
        padding: 0;
    }

    .app-home {
        .app-header {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;

            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 0 20px;
            box-sizing: border-box;
            border-bottom: solid 1px #f2f2f2;
            background-color: white;
        }

        .app-menu {
            position: fixed;
            left: 0;
            bottom: 0;
            border-right: solid 1px #f2f2f2;
            background-color: white;
        }

        .app-content {
            min-height: 100vh;
            box-sizing: border-box;
        }
    }
</style>
```

**app-menu.vue核心代码**

关键在于点击菜单的时候，调用 `navigator.methods.go()`跳转，参数就是页面文件以pages为根目录的绝对地址；

```html
<script lang="ts">
    import {injectAppNavigator} from "../navigator/app-navigator";
    import {AppMenu, MENUS} from "./menus";

    export default {
        name: "app-menu",
        props: {
            currentPath: {type: String},
        },
        setup() {
            const navigator = injectAppNavigator()

            return {
                menus: MENUS,
                handleClickMenu(menu: AppMenu) {
                    navigator.methods.go(menu.page)
                },
            }
        },
    }
</script>
```

#### 4、实现效果

![navigator.gif](./navigator.gif)

- 当点击某一个菜单的时候，能够显示对应页面的内容；
- 刷新浏览器页签，仍然能够正确加载当前url地址所链接的页面；
- 使用浏览器前进后退功能正确加载页面；
- 刷新浏览器页签，观察浏览器控制台network，当点击菜单，会加载对应页面文件的chunk文件，除非刷新浏览器，否则页面的chunk文件只会加载一次，实现按需加载页面的功能；

## 作业

- 解释 shims-vue.d.ts的作用、使用`.vue`文件开发组件与tsx开发组件之间的区别优缺点；
- 完成app等首页组件，实现点击菜单跳转，并且实现【实现路由组件】章节中最后描述的效果；

> 在下一阶段发布实践文档的时候，会公布本次实践的代码工程源码。