# DAY01

本次实践主要内容是：使用 `vue-cli`创建一个 `Vue3.0`工程，打包，并且部署到`gitee`的`Page`服务上；

## 创建工程

比如组件库的名字叫做：`v3-components`

```bash
vue create v3-components
```

> 确保你的vue-cli脚手架是比较新的版本，可以通过 `npm i @vue/cli -g` 安装为最新的正式版本；



`vue create` 命令执行完之后，可以看到如下三个选项：

- Default([Vue 2] babel, eslint)
- Default(Vue 3 Preview)([Vue 3] babel,eslint)
- Manually select features

这里我们选择第三个选项，手动选择我们需要的特性。接着我们勾选如下选项：

- [x] Choose Vue version
- [x] Babel
- [x] Typescript
- [ ] Progressive Web App (PWA) Support
- [ ] Router
- [x] Vuex
- [x] Css Pre-processers 
- [x] Linter / Formatter
- [ ] Unit Testing
- [ ] E2E Testing

---

接着回车下一步，勾选使用Vue3.0

- [ ] 2.x
- [x] 3.x

--- 

然后是 `Use class-style component syntax`，这个就用默认的选项`N`，直接回车就行；

> 这个意思是是否使用 class 风格的组件语法，就是使用 class 类加上 `vue-property-decorator` 中的装饰器来编写组件，这种写法我感觉没有必要学了，直接略过；

--- 

然后是 `Use babel alongside typescript`，使用默认选项 Yes；

---

`Pick a Css pre-processor`选择css预处理语言，这里我们选择 `Sass/Scss(with dart-sass)`

---

Eslint选择默认选项就好了，之后的就是一直回车直到结束；安装依赖完毕之后，进入工程 `cd v3-components`，然后启动本地服务：`npm run serve`；

## 问题解答

### 为什么不使用vite？

Vue2.0 的jsx编译插件，只能使用v-model指令，其他指令得通过渲染函数原生的方式来写，到了Vue3.0，jsx-next文档中明确指出可以在jsx中使用指令语法，并且支持自定义指令。可问题是vite创建的工程我发现好像不能使用指令语法，为了能够有更好，更全面地的`Vue3.0`使用体验，我们使用 `vue-cli`来创建组件库工程；

---

vite创建的工程默认没有配置 typescript 所需要的一些文件，比如:

- tsconfig.json
- src/shims-vue.d.ts

在vite工程中还是手动加上这两个文件，typescript才能够正常的工作；

---

vite中配置alias有点奇葩，而且因为浏览器基于es module引入的原因，vite中好像还不支持extension（自动给文件添加后缀名）

> 主要是由于第一个原因，目前还是先不用vite，等vite更成熟的时候再做转换；

### 在 select features 的时候为什么要选择 vuex？

我们组件库是不依赖于vuex的，引入vuex只是测试我们的组件库部分组件功能是否正常。比如我们会开发一个 `$dialog` 弹框服务组件。当开发者使用服务调用弹框时，我们需要确保弹框内的组件能够正常通过 this 访问vuex创建的全局状态，毕竟 $dialog 服务是动态挂载组件提供服务的，要确保不会丢失组件上下文的一些状态；

### 有的学员更熟悉 stylus或者less，可以不使用sass嘛？

我劝你最好还是使用sass，因为 plain-ui 在实现主题化的时候使用了有点怪异的写法，转换成less或者stylus可能会比较吃力；当然如果你有成功的组件库主题化实践思路，也是可以的。

## 打包部署

先在码云上创建一个空的仓库，用来存放这个工程代码。比如我创建完了之后，就得到了这个地址：[git@gitee.com:zhufeng-training/v3-components.git](git@gitee.com:zhufeng-training/v3-components.git)，我们在这个工程里面添加上这个远程仓库地址；

接着工程根目录下创建一个打包配置文件：`vue.config.js`；内容如下所示：

```js
module.exports = {
    outputDir: 'docs',
    publicPath: '/v3-components/',
}
```

接着执行打包命令：`npm run build`；然后工程根目录下就会出现docs目标文件夹。我们将这些修改后的所有文件都push到远程仓库中。

之后打开码云上面，仓库的主页，点击`服务`选项下的 `Gitee Pages`，部署目录输入 `docs`。接着发布成功后，码云会给出
访问地址，访问地址能够正常打开页面表示部署成功；

## 问题解答

#### outputDir可不可以换成别的目录？

可以，最终gitee pages里面部署的目录就是你换的目录。

#### publicPath是什么，有什么意义？

- 如果你还不知道publicPath是什么，那么你应该马上停止训练营，赶紧恶补
张老师和姜老师讲解的路由相关知识；
- 接下来要实现的简易路由，以及组件库中的页面路由组件都跟publicPath有关系，你应该整理一份 history路由以及hash路由相关的文档，做好知识储备；

以这个示例仓库为例，部署成功之后给的路径是：[http://zhufeng-training.gitee.io/v3-components](http://zhufeng-training.gitee.io/v3-components)

实际上，当你看到这个路径的时候就应该知道，publicPath是v3-components，这个部署的路径是gitee给的，在我们无法控制的情况下只能设置publicPath为v3-components，如果你可以在自己的机器使用nginx之类的部署，那么可以配置nginx一级目录代理到你打包好的文件目录，这样你打包的时候就不需要配置 publicPath了。

## 作业

- 将工程打包部署到 gitee，并且能够正常访问页面（每写一个组件，就部署到码云上，可以观察到自己在一点一点成长）；
- (可选)整理history路由以及hash路由相关文档（知识储备）；
- (可选)vue-cli 与 vite的区别，各自的适用场景（结合webpack、rollup、tree-shaking等知识整理）；